using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Selenium.WebDriver.Extensions.QuerySelector;
using OpenQA.Selenium.Support.UI;
using By = OpenQA.Selenium.By;

namespace ProjetoUnit
{
    [TestFixture]
    public class Tests
    {
        public bool True { get; private set; }

        [SetUp]
        public void Setup()
        {
         }
        
        [Test]
        public void Test1()
        {
         bool IsLoggedIn = True ;
            Assert.IsTrue(IsLoggedIn.Equals("True"));
        }
    
        
        [Test]
        public void Test2()
        {
         string tamanho = "tres";
            Assert.IsFalse(tamanho.Equals("dois"));
        }

         [Test]
        public void Test3()
        {
         string tamanho = "dois";
            Assert.AreEqual("dois",tamanho);
        }

        [Test]
        public void instanciarDriver()
        {
            string titulo = "https://www.google.com.br/";
            //string name = "q";

            IWebDriver driver = new ChromeDriver("C://Users//talineLopes//Desktop");

            driver.Navigate().GoToUrl(titulo);
            Assert.AreEqual(driver.Url, titulo);
            Assert.AreEqual(driver.Title, "Google");

            By elementoX = By.Name("q");
            IWebElement elemento = driver.FindElement(elementoX);
            elemento.SendKeys("Yotube");
            Assert.AreEqual(elemento, "Youtube");

            driver.Quit();

        }

        [Test]
        public void site_teste()
        {
            string titulo = "https://ultimateqa.com/filling-out-forms/";

            IWebDriver driver = new ChromeDriver("C://Users//talineLopes//Desktop");

            driver.Navigate().GoToUrl(titulo);

            By elementoX = By.Id("et_pb_contact_name_1");
            IWebElement elemento = driver.FindElement(elementoX);
            elemento.SendKeys("Taline");
            Assert.AreEqual(elemento, "Taline");
            
            By elementoX2 = By.Id("et_pb_contact_message_1");
            IWebElement elemento2 = driver.FindElement(elementoX2);
            elemento2.SendKeys("Ola");
            Assert.AreEqual(elemento2, "Ola");

            By elementoX3 = By.ClassName("input et_pb_contact_captcha");
            IWebElement elemento3 = driver.FindElement(elementoX3);
            elemento3.SendKeys("5");
            Assert.AreEqual(elemento3, "5");

            By elementoX4 = By.XPath("//div[@id='et_pb_contact_form_1']//button[@name='et_builder_submit_button']");
            IWebElement elemento4 = driver.FindElement(elementoX4);
            Click(elemento4);

            driver.Quit();
    
        }

        private void Click(IWebElement elemento4)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void site2()
        {
            string titulo = "http://the-internet.herokuapp.com/dropdown";

            IWebDriver driver = new ChromeDriver("C://Users//talineLopes//Desktop");

            driver.Navigate().GoToUrl(titulo);

            By elementoX = By.Id("dropdown");
            IWebElement elemento = driver.FindElement(elementoX);
            SelectElement combobox = new SelectElement(elemento);
            combobox.SelectByValue("2");
            driver.Quit();
    
        }

        
        [Test]
        [Obsolete]
        public void Meu_caso_teste()
        {
            string titulo = "http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth";

            ChromeOptions options = new ChromeOptions();
    
            options.AddArgument("start-maximized");

            IWebDriver driver = new ChromeDriver("C://Users//talineLopes//Desktop",options);
        

             driver.Navigate().GoToUrl(titulo);

             WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));

             //cookie
             By elementoX8 = By.ClassName("cc-btn cc-dismiss");
            IWebElement elemento8 = driver.FindElement(elementoX8);
            Click(elemento8);


                ///login
            By elementoX1 = By.Id("login");
            IWebElement elemento1 = driver.FindElement(elementoX1);
            elemento1.SendKeys("taline.moreira@base2.com.br");

            By elementoX2 = By.Id("password");
            IWebElement elemento2 = driver.FindElement(elementoX2);
            elemento2.SendKeys("12102018y");
        
            By elementoX3 = By.ClassName("btn-crowdtest");
            IWebElement elemento3 = driver.FindElement(elementoX3);
            Click(elemento3);


            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("btn btn-crowdtest mr-1")));
            
         By elementoX5 = By.ClassName("btn btn-crowdtest mr-1");
            IWebElement elemento5 = driver.FindElement(elementoX5);
            Click(elemento5);
            
            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("crowd-icon")));

            By elementoX6 = By.ClassName("crowd-icon");
            IWebElement elemento6 = driver.FindElement(elementoX6);
            Click(elemento6);

            wait.Until(ExpectedConditions.ElementToBeClickable( By.ClassName("mat-cell cdk-column-identifier mat-column-identifier ng-star-inserted")));
            By elementoX7 = By.ClassName("mat-cell cdk-column-identifier mat-column-identifier ng-star-inserted");
            IWebElement elemento7 = driver.FindElement(elementoX7);
            Click(elemento7);

            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("mat-tab-label-content")));
            By elementoX9 = By.ClassName("mat-tab-label-content");
            IWebElement elemento9 = driver.FindElement(elementoX9);
            Click(elemento9);

            wait.Until(ExpectedConditions.ElementExists(By.ClassName("mat-row ng-star-inserted")));

            Boolean isPresent = driver.FindElements(By.ClassName("mat-row ng-star-inserted")).Count > 0;

             driver.Quit();    
    
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {

            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

}
